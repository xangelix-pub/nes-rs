use bitflags::bitflags;

// The `bitflags!` macro generates `struct`s that manage a set of flags.
// bitflags! {
//     #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
//     struct LDA: u8 {
//         const IMMEDIATE = 0xA9;
//     }
// }

fn main() {
    println!("Hello, world!");
}

#[derive(Clone, Copy, Debug, Default)]
#[allow(non_camel_case_types)]
pub enum AddressingMode {
    #[default]
    NoneAddressing,
    Immediate,
    ZeroPage,
    ZeroPage_X,
    ZeroPage_Y,
    Absolute,
    Absolute_X,
    Absolute_Y,
    Indirect_X,
    Indirect_Y,
}

#[derive(Clone, Copy, Debug, Default)]
enum InstructionType {
    #[default]
    None,
    ADC,
    AND,
    ASL,
    BCC,
    BCS,
    BEQ,
    BIT,
    BMI,
    BNE,
    BPL,
    BRK,
    BVC,
    BVS,
    CLC,
    CLD,
    CLI,
    CLV,
    CMP,
    CPX,
    CPY,
    DEC,
    DEX,
    DEY,
    EOR,
    INC,
    INX,
    INY,
    JMP,
    JSR,
    LDA,
    LDX,
    LDY,
    LSR,
    NOP,
    ORA,
    PHA,
    PHP,
    PLA,
    PLP,
    ROL,
    ROR,
    RTI,
    RTS,
    SBC,
    SEC,
    SED,
    SEI,
    STA,
    STX,
    STY,
    TAX,
    TAY,
    TSX,
    TXA,
    TXS,
    TYA,
}

#[derive(Clone, Copy, Default)]
pub struct Instruction {
    instruction: InstructionType,
    mode: AddressingMode,
    bytes: u8,
    cycles: u8,
}

pub struct CPU {
    pub register_a: u8,
    pub register_x: u8,
    pub register_y: u8,
    pub status: u8,
    pub program_counter: u16,
    pub memory: [u8; 0xFFFF],
    pub instruction_set: [Instruction; 256],
}

fn load_6502(cpu: &mut CPU) {
    if cpu.instruction_set.len() != 256 {
        panic!("Instruction set memory must be 256 bytes long.");
    }
    // BRK
    cpu.instruction_set[0x00] = Instruction {
        instruction: InstructionType::BRK,
        mode: AddressingMode::Immediate,
        bytes: 1,
        cycles: 7,
    };
    // LDA
    cpu.instruction_set[0xA9] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::Immediate,
        bytes: 2,
        cycles: 2,
    };
    cpu.instruction_set[0xA5] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::ZeroPage,
        bytes: 2,
        cycles: 3,
    };
    cpu.instruction_set[0xB5] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::ZeroPage_X,
        bytes: 2,
        cycles: 4,
    };
    cpu.instruction_set[0xAD] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::Absolute,
        bytes: 3,
        cycles: 4,
    };
    cpu.instruction_set[0xBD] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::Absolute_X,
        bytes: 3,
        cycles: 4,
    };
    cpu.instruction_set[0xB9] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::Absolute_Y,
        bytes: 3,
        cycles: 4,
    };
    cpu.instruction_set[0xA1] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::Indirect_X,
        bytes: 2,
        cycles: 6,
    };
    cpu.instruction_set[0xB1] = Instruction {
        instruction: InstructionType::LDA,
        mode: AddressingMode::Indirect_Y,
        bytes: 2,
        cycles: 5,
    };
    // STA
    cpu.instruction_set[0x85] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::ZeroPage,
        bytes: 2,
        cycles: 3,
    };
    cpu.instruction_set[0x95] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::ZeroPage_X,
        bytes: 2,
        cycles: 4,
    };
    cpu.instruction_set[0x8D] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::Absolute,
        bytes: 3,
        cycles: 4,
    };
    cpu.instruction_set[0x9D] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::Absolute_X,
        bytes: 3,
        cycles: 5,
    };
    cpu.instruction_set[0x99] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::Absolute_Y,
        bytes: 3,
        cycles: 5,
    };
    cpu.instruction_set[0x81] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::Indirect_X,
        bytes: 2,
        cycles: 6,
    };
    cpu.instruction_set[0x91] = Instruction {
        instruction: InstructionType::STA,
        mode: AddressingMode::Indirect_Y,
        bytes: 2,
        cycles: 6,
    };
    // TAX
    cpu.instruction_set[0xAA] = Instruction {
        instruction: InstructionType::TAX,
        mode: AddressingMode::NoneAddressing,
        bytes: 1,
        cycles: 2,
    };
    // INX
    cpu.instruction_set[0xE8] = Instruction {
        instruction: InstructionType::INX,
        mode: AddressingMode::NoneAddressing,
        bytes: 1,
        cycles: 2,
    };
}

impl Default for CPU {
    fn default() -> Self {
        CPU::new()
    }
}

impl CPU {
    pub fn new() -> Self {
        CPU {
            register_a: 0,
            register_x: 0,
            register_y: 0,
            status: 0,
            program_counter: 0,
            memory: [0; 0xFFFF],
            instruction_set: [Instruction::default(); 256],
        }
    }

    fn mem_read(&self, addr: u16) -> u8 {
        *self
            .memory
            .get(usize::from(addr))
            .expect("Attempted to read memory out of bounds.")
    }

    fn mem_write(&mut self, addr: u16, data: u8) {
        if self.memory.len() < usize::from(addr) {
            panic!("Attempted to write memory out of bounds.");
        }
        self.memory[usize::from(addr)] = data;
    }

    fn mem_read_u16(&mut self, pos: u16) -> u16 {
        u16::from_le_bytes([
            self.mem_read(pos),
            self.mem_read(pos.checked_add(1).expect("Little-endian read overflow.")),
        ])
    }

    fn mem_write_u16(&mut self, pos: u16, data: u16) {
        let le = u16::to_le_bytes(data);
        self.mem_write(
            pos,
            *le.first()
                .expect("First byte from little-endian could not be read."),
        );
        self.mem_write(
            pos + 1,
            *le.get(1)
                .expect("Second byte from little-endian could not be read."),
        );
    }

    pub fn reset(&mut self) {
        self.register_a = 0;
        self.register_x = 0;
        self.register_y = 0;
        self.status = 0;

        self.program_counter = self.mem_read_u16(0xFFFC);
    }

    pub fn load_and_run(&mut self, program: Vec<u8>) {
        load_6502(self);
        //println!("{:?}", self.instruction_set[0x00].instruction);
        self.load(program);
        self.reset();
        self.run();
    }

    pub fn load(&mut self, program: Vec<u8>) {
        self.memory[0x8000..(0x8000 + program.len())].copy_from_slice(&program[..]);
        self.mem_write_u16(0xFFFC, 0x8000);
    }

    pub fn update_zero_and_negative_flags(&mut self, result: u8) {
        if result == 0 {
            self.status |= 0b0000_0010;
        } else {
            self.status &= 0b1111_1101;
        }

        if result & 0b1000_0000 != 0 {
            self.status |= 0b0100_0000;
        } else {
            self.status &= 0b1011_1111;
        }
    }

    fn get_operand_address(&mut self, mode: &AddressingMode) -> u16 {
        match mode {
            AddressingMode::Immediate => self.program_counter,
            AddressingMode::ZeroPage => u16::from(self.mem_read(self.program_counter)),
            AddressingMode::ZeroPage_X => {
                let addr = u16::from(self.mem_read(self.program_counter));
                addr.wrapping_add(u16::from(self.register_x))
            }
            AddressingMode::ZeroPage_Y => {
                let addr = u16::from(self.mem_read(self.program_counter));
                addr.wrapping_add(u16::from(self.register_y))
            }
            AddressingMode::Absolute => self.mem_read_u16(self.program_counter),
            AddressingMode::Absolute_X => {
                let addr = self.mem_read_u16(self.program_counter);
                addr.wrapping_add(u16::from(self.register_x))
            }
            AddressingMode::Absolute_Y => {
                let addr = self.mem_read_u16(self.program_counter);
                addr.wrapping_add(u16::from(self.register_y))
            }
            AddressingMode::Indirect_X => {
                let short_addr = self
                    .mem_read(self.program_counter)
                    .wrapping_add(self.register_x);
                let deref = u16::from_le_bytes([
                    self.mem_read(u16::from(short_addr)),
                    self.mem_read(u16::from(
                        short_addr
                            .checked_add(1)
                            .expect("Little-endian read overflow."),
                    )),
                ]);
                deref.wrapping_add(u16::from(self.register_y))
            }
            AddressingMode::Indirect_Y => {
                let short_addr = self.mem_read(self.program_counter);
                let deref = u16::from_le_bytes([
                    self.mem_read(u16::from(short_addr)),
                    self.mem_read(u16::from(
                        short_addr
                            .checked_add(1)
                            .expect("Little-endian read overflow."),
                    )),
                ]);
                deref.wrapping_add(u16::from(self.register_y))
            }
            AddressingMode::NoneAddressing => todo!(),
        }
    }

    fn lda(&mut self, mode: &AddressingMode) {
        let addr = self.get_operand_address(mode);
        let value = self.mem_read(addr);

        self.register_a = value;
        self.update_zero_and_negative_flags(self.register_a);
    }

    fn tax(&mut self) {
        self.register_x = self.register_a;
        self.update_zero_and_negative_flags(self.register_x);
    }

    fn inx(&mut self) {
        // Bug for bug?
        // self.register_x = self
        //         .register_x
        //         .checked_add(1)
        //         .expect("Register X overflow.");
        self.register_x = self.register_x.wrapping_add(1);
        self.update_zero_and_negative_flags(self.register_x);
    }

    fn sta(&mut self, mode: &AddressingMode) {
        let addr = self.get_operand_address(mode);
        self.mem_write(addr, self.register_a);
    }

    fn bump_program_counter(&mut self, amount: u16) {
        self.program_counter = self
            .program_counter
            .checked_add(amount)
            .expect("Program counter overflow.");
    }

    pub fn run(&mut self) {
        loop {
            let opscode = self.mem_read(self.program_counter);
            self.bump_program_counter(1);

            let instruction = self.instruction_set[usize::from(opscode)];
            println!("{:?}", instruction.instruction);
            println!("{:?}", self.instruction_set[0x00].instruction);
            match instruction.instruction {
                InstructionType::BRK => return,
                InstructionType::LDA => self.lda(&instruction.mode),
                InstructionType::TAX => self.tax(),
                InstructionType::INX => self.inx(),
                InstructionType::STA => self.sta(&instruction.mode),
                _ => panic!("Unimplemented opscode: {:#X}", opscode),
            }

            self.bump_program_counter(u16::from(instruction.bytes - 1));
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_lda_from_memory() {
        let mut cpu = CPU::new();
        cpu.mem_write(0x10, 0x55);

        cpu.load_and_run(vec![0xa5, 0x10, 0x00]);

        assert_eq!(cpu.register_a, 0x55);
    }

    #[test]
    fn test_0xa9_lda_immediate_load_data() {
        let mut cpu = CPU::new();
        cpu.load_and_run(vec![0xa9, 0x05, 0x00]);
        assert_eq!(cpu.register_a, 0x05);
        assert!(cpu.status & 0b0000_0010 == 0b00);
        assert!(cpu.status & 0b1000_0000 == 0);
    }

    #[test]
    fn test_0xa9_lda_zero_flag() {
        let mut cpu = CPU::new();
        cpu.load_and_run(vec![0xa9, 0x00, 0x00]);
        assert!(cpu.status & 0b0000_0010 == 0b10);
    }

    #[test]
    fn test_0xaa_tax_move_a_to_x() {
        let mut cpu = CPU::new();
        cpu.load_and_run(vec![0xa9, 0x0a, 0xaa, 0x00]);

        assert_eq!(cpu.register_x, 10)
    }

    #[test]
    fn test_5_ops_working_together() {
        let mut cpu = CPU::new();
        cpu.load_and_run(vec![0xa9, 0xc0, 0xaa, 0xe8, 0x00]);

        assert_eq!(cpu.register_x, 0xc1)
    }

    #[test]
    fn test_inx_overflow() {
        let mut cpu = CPU::new();
        cpu.load_and_run(vec![0xa9, 0xff, 0xaa, 0xe8, 0xe8, 0x00]);

        assert_eq!(cpu.register_x, 1)
    }
}
